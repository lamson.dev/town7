import { Navigation } from "react-native-navigation";
import { Icon } from "react-native-elements";

const selectedIconColor = '#0089da';
// const

export const goToLogin = () => Navigation.setRoot({
    root: {

        stack: {
            id: "stackMain",
            children: [
                {

                    component: {
                        name: "LoginScreen",
                        options: {
                            topBar: {
                                visible: false
                            },
                            // statusBar:{
                            //   backgroundColor: 'red',
                            //   style: "dark"
                            // }
                        }
                    },

                }
            ]
        }
    }
});

export const showSideTabsMenu = (userData = null) => {

    Navigation.setRoot({
        root: {
            bottomTabs: {
                id: 'MainAppBottomTab',
                children: [

                    {
                        stack: {
                            id: "homeTab",
                            children: [
                                {
                                    component: {
                                        name: 'HomeScreen',
                                        options: {
                                            bottomTab: {
                                                text: "Home",
                                                icon: require('../assets/images/icons/home-ic.png'),
                                                selectedIconColor,
                                            },
                                            topBar: {
                                                visible: false,
                                                drawBehind: true,
                                                background: {
                                                    color: 'transparent'
                                                },
                                                noBorder: true,
                                                elevation: 0,
                                                leftButtons: {
                                                    id: 'btn_toggle_drawer_side',
                                                    name: 'BTN_TOGGLE_DRAWER',
                                                    icon: require('../assets/images/icons/menu-ic.png'),
                                                    //   icon: require('../assets/images/icons/menu-ic.png'),
                                                    color: '#fff',
                                                }

                                            }
                                        },
                                    },

                                }
                            ],
                            options: {
                                bottomTab: {
                                    text: 'Tab Name',
                                },

                            }
                        }
                    },

                    {
                        stack: {
                            id: 'notificationTab',
                            children: [
                                {
                                    component: {
                                        name: 'NotificationScreen',
                                        options: {

                                            topBar: {
                                                visible: false,
                                            }
                                        },
                                        passProps: {
                                            // userDataProps: userData ? userData : 'Undefine',
                                        }
                                    },

                                }
                            ],
                            options: {
                                bottomTab: {
                                    text: "Notifications",
                                    icon: require('../assets/images/icons/notification-ic.png'),
                                    selectedIconColor
                                },
                            }
                        }
                    },

                    {
                        stack: {
                            id: 'userTab',
                            children: [

                                {
                                    component: {
                                        name: 'UserProfileScreen',
                                        id: "UserProfileScreenId",
                                        options: {
                                            topBar: {
                                                visible: false,
                                                drawBehind: true,
                                                background: {
                                                    color: 'transparent',
                                                },

                                            },
                                        },
                                    },

                                },

                            ],
                            options: {
                                bottomTab: {
                                    text: "Profile",
                                    icon: require('../assets/images/icons/user-ic.png'),
                                    selectedIconColor
                                },
                            }
                        }
                    },

                ],

                options: {
                    bottomTabs: {
                        // backgroundColor: 'green'
                    },
                }
            },
        }
    });
}
