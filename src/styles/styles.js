import { StyleSheet } from "react-native";

export const mStyles = StyleSheet.create({
    bkgColorMain: {
        // backgroundColor: "#F2F2F2"
        // backgroundColor: "#e3e3e3"
        backgroundColor: "#fff"
    },
    textMainTitle: {
        fontSize: 25,
        fontWeight: 'bold',
        color: '#24253D',
        textAlign: 'center',
    },
    mrgBottMainTitle:{
        marginBottom: 25
    },
    viewBtnMain:{
        marginHorizontal: 20,
        marginTop: 30,
        marginBottom: 25
    },
    
    viewTextBelowBtnMain:{
        flexDirection: 'row',
        justifyContent: 'center',
    },
    textBelowBtnMainLeft:{
        textAlign: 'center',
        marginRight: 5
    },
    textBelowBtnMainRight:{
        color: '#24c3f0',
        fontWeight: 'bold'   
    }
});