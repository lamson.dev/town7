import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView, } from "react-native";
import { Icon } from "react-native-elements";
import { Field, reduxForm } from "redux-form";

class EditUserForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: props.userDataProps
        }
    }

    renderInput = ({ lableField, secureTextEntry, maxLength, keyboardType, placeholder, value, defaultValue, editable = true, input: { onChange, ...restInput }, meta: { touched, error } }) => {
        return (
            <View style={styles.viewFieldEdit}>
                <Text style={styles.textLabelField}>
                    {lableField}
                </Text>
                {(touched && error) && <Text>{error}</Text>}
                <TextInput style={styles.inputEditField}
                    {...restInput}
                    placeholder={placeholder}
                    maxLength={maxLength}
                    keyboardType={keyboardType}
                    secureTextEntry={secureTextEntry}
                    defaultValue={defaultValue}
                    value={value}
                    editable={editable}
                    onChangeText={onChange} />

            </View>
        )
    }

    render() {
        const { userData, handleSubmit } = this.props;
        return (
            <View style={[styles.viewMain]}>
                <View style={styles.viewTop}>
                    {/* avatar, name, address */}
                    <View style={styles.viewAvt}>
                        <Image style={styles.imageAvt} source={{ uri: 'https://scontent.fdad1-1.fna.fbcdn.net/v/t1.0-9/p720x720/82188085_231976127789978_1617989561374212096_o.jpg?_nc_cat=100&_nc_sid=0be424&_nc_ohc=1WqDoOV7OmgAX-POMD4&_nc_ht=scontent.fdad1-1.fna&_nc_tp=6&oh=9de30cdb52cb1d4eb1951c39fa2ed09d&oe=5E92EFA4' }} />
                        <View style={styles.viewIconCamera}>
                            <TouchableOpacity>
                                <Icon
                                    name='camera'
                                    type='feather'
                                    color="#354245"
                                    size={50}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                {/* Fields editing user information */}
                <View style={styles.viewEditForm}>
                    <ScrollView>


                        {/* Field */}
                        <Field
                            name={'username'}
                            lableField={'Username'}
                            defaultValue={this.state.userData.username}
                            editable={false}
                            component={this.renderInput} />
                        <Field
                            name={'fullName'}
                            lableField={'Full Name'}
                            defaultValue={this.state.userData.fullName}
                            component={this.renderInput} />
                        <Field
                            name={'email'}
                            lableField={'Email'}
                            defaultValue={this.state.userData.email}
                            component={this.renderInput} />
                        <Field
                            name={'phone'}
                            lableField={'Phone'}
                            defaultValue={this.state.userData.phone}
                            keyboardType={'phone-pad'}
                            component={this.renderInput} />
                        <Field
                            name={'national'}
                            lableField={'National'}
                            defaultValue={this.state.userData.national}
                            component={this.renderInput} />
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        // justifyContent: 'center'
    },
    viewTop: {
        backgroundColor: '#f6f7f9',
        paddingHorizontal: 12.5,
        paddingTop: 15,
        paddingBottom: 15
    },
    viewAvt: {
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 80,
        width: 145,
        height: 145,

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.37,
        shadowRadius: 5.49,

        elevation: 2,

    },
    imageAvt: {
        width: "100%",
        height: "100%",
        borderRadius: 80,
        borderWidth: 2,
        position: "relative",
        opacity: 0.5

    },
    viewIconCamera: {
        position: 'absolute',
        bottom: 2,
    },

    viewEditForm: {
        backgroundColor: '#ffff',
        flex: 1,
        paddingTop: 15
    },

    viewFieldEdit: {
        marginHorizontal: 15,
        marginVertical: 15,
        borderBottomWidth: 1.35,
        borderColor: '#e8e8e8',
        // backgroundColor: 'blue'
    },
    textLabelField: {
        color: '#82817f',
        fontSize: 14.5
    },
    inputEditField: {
        paddingLeft: 0,
        fontWeight: 'bold',
        fontSize: 17
    }
});

export default reduxForm({
    form: 'editUser',
    enableReinitialize: true

})(EditUserForm);