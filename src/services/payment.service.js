import { fetchApi } from "./api/api";

class PaymentService {
    async momoPayment(data) {
        console.log(data);
        return await fetchApi("/momo/basic-info", "POST", data);
    }
}

export default new PaymentService();