import { AsyncStorage, YellowBox } from "react-native";
// import AsyncStorage from '@react-native-community/async-storage';

YellowBox.ignoreWarnings(["Warning: AsyncStorage has been extracted from react-native core and will be removed in a future release. It can now be installed and imported from '@react-native-community/async-storage' instead of 'react-native'. See https://github.com/react-native-community/react-native-async-storage"]);

const deviceStorage = {

    // store user id
    async  storeUserId(userId) {
        try {
            console.log('store userId: ',userId);
            await AsyncStorage.setItem('userId', userId.toString());
        } catch (error) {
            console.log('Store userId error: ', error);
        }
    },

    async  loadUserId() {
        try {
            return AsyncStorage.getItem('userId');
        } catch (error) {
            console.log('Load userId error: ', error);
        }
    },

    async  removeUserId() {
        try {
            await AsyncStorage.removeItem('userId');
            console.log('UserId was removed');
        } catch (error) {
            console.log('Remove token error: ', error);
        }
    },

    // store token
    async  storeToken(token) {
        try {
            await AsyncStorage.setItem('token', token);
        } catch (error) {
            console.log('Store token error: ', error);
        }
    },

    async  loadToken() {
        try {
            return AsyncStorage.getItem('token');
        } catch (error) {
            console.log('Load token error: ', error);
        }
    },

    async  removeToken() {
        try {
            await AsyncStorage.removeItem('token');
            console.log('Token was removed');
        } catch (error) {
            console.log('Remove token error: ', error);
        }
    }

};

export default deviceStorage;