import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";

class bookingRoomService {

    async booking(data) {
        const authToken = await deviceStorage.loadToken();
        return fetchApi("/bookings", "POST", data, authToken);
    }

    async getAll() {
        const authToken = await deviceStorage.loadToken();
        return fetchApi("/users/booking-histories", "GET", null, authToken);
    }
    async getById(id) {
        const authToken = await deviceStorage.loadToken();
        return fetchApi("/bookings/"+id, "GET", null, authToken);
    }
}

export default new bookingRoomService();