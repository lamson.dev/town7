import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";

class NotificationService {

    async getAll() {
        return await fetchApi("/notifications/users", "GET", null, await deviceStorage.loadToken());
    }

    async read(id) {
        return await fetchApi("/notifications/users/" + id, "PUT", null, await deviceStorage.loadToken());
    }

}

export default new NotificationService();
