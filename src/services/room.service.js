import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";
class RoomService {

    async getAll() {
        return await fetchApi("/hosts/", "GET");
    }

    async getById(id) {
        return await fetchApi("/hosts/" + id, "GET");
    }
    
    async getByRecommendation() {
        return await fetchApi("/hosts/recommendations", "GET", null, await deviceStorage.loadToken());
    }

    async getDateLook(id) {
        return await fetchApi("/hosts/users/dates-look/"+id, "GET", null, await deviceStorage.loadToken());
    }

    async getByCity(city_id) {
        return await fetchApi("/hosts/get-by-city-id/" + city_id, "GET", null);
    }

    async searchRoom(data) {
        return await fetchApi("/hosts/search?keyword=" + data, "GET");
    }
}

export default new RoomService();