import { fetchApi } from "./api/api";
import deviceStorage from "./deviceStorage.service";

class UserService {

    async getMe() {
        return fetchApi("/users/me", "GET", null, await deviceStorage.loadToken());
    }

}

export default new UserService();