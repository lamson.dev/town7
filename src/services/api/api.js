import axios from "axios";
const BASE_URL = "https://town-house-api-seven-team.herokuapp.com/api";

export const fetchApi = async (uri, method, body = null, token = null) => {

    const url = BASE_URL.concat(uri);

    const reqBody = body ? JSON.stringify(body) : null;

    const redHeaders = {
        // "Content-type": "application/json",
        "Accept": "application/json",
        "Authorization": token ? "Bearer " + token : null
    };
    const fetchParams = { url, method }

    // fetchParams.headers["Content-type"] = "application/json";
    // fetchParams.headers["Accept"] = "application/json";
    // fetchParams.headers["Authorization"] = "Bearer " + token;

    fetchParams.headers = redHeaders;

    if (reqBody) {
        fetchParams.data = body;
    }

    // console.log('fetch params: ', fetchParams);

    return axios(fetchParams)
        .then(function (response) {
            return response;
        });
}
