import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements";

export default class CardHostList extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { hostName, imgUri, price, starVoting, reviewTotal, address, onPress, bedroomCount } = this.props;
        return (
            <TouchableOpacity onPress={onPress}>
                <View style={styles.viewCardList}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                        {/* name room, stars, total review, price */}
                        <View style={styles.viewPartCardList}>
                            <Text style={styles.textNameRoom} numberOfLines={2}>
                                {hostName}
                            </Text>

                            {/* stars and total reviews */}
                            <View style={styles.viewStarReview}>
                                {/* Number of stars */}

                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={styles.textNumStar}>
                                        {starVoting}
                                    </Text>
                                    <Icon
                                        name='stars'
                                        type='MaterialIcons'
                                        color="#1EA896"
                                        size={15}
                                    />
                                </View>

                                {/* Number of total review */}
                                <Text style={styles.textNumReview}>
                                    {reviewTotal} Review
                            </Text>

                            </View>

                            <View style={styles.viewSubBottom}>
                                <Icon
                                    name="bed"
                                    type="font-awesome"
                                    color="#9ea0a4"
                                    size={15}
                                />
                                <Text style={styles.textSubBottomProperty}>
                                    {bedroomCount} bed
                                </Text>
                            </View>
                        </View>

                        {/* room image, wish button */}
                        <View style={styles.viewPartCardList}>
                            <Image style={styles.imageRoom} source={{ uri: imgUri }} />
                            {/* Price */}
                            <Text style={styles.textPrice}>
                                {price} VND
                          </Text>
                        </View>

                    </View>

                    <View style={styles.viewAddressText}>
                        <Icon
                            style={styles.iconRight}
                            name='location-on'
                            type='materialIcons'
                            color="#9ea0a4"
                            size={15}
                        />
                        <Text style={styles.textAddress} numberOfLines={1}>
                            {address}
                        </Text>
                    </View>

                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({

    viewCardList: {
        marginHorizontal: 10,
        minHeight: 146,
        // flexDirection: 'row',

        backgroundColor: '#ffff',
        paddingHorizontal: 20,
        marginVertical: 2.5,
        borderRadius: 10,
        shadowColor: "rgba(0,0,0,0.75)",
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowOpacity: 0.29,
        shadowRadius: 3.84,
        elevation: 7,
    },

    viewPartCardList: {
        paddingTop: 10,
    },
    textNameRoom: {
        maxWidth: 200,
        fontSize: 16,
        fontStyle: 'normal',
        fontWeight: "700",
        lineHeight: 19,
        color: '#525252',

    },
    viewStarReview: {
        flexDirection: 'row',
        paddingVertical: 9
    },
    textNumStar: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#1EA896',
        marginRight: 2.5
    },
    textNumReview: {
        marginLeft: 2.5,
        fontStyle: "italic",
        fontSize: 13.5,
        color: '#474D60',
    },
    textPrice: {
        fontSize: 20,
        fontStyle: 'normal',
        textAlign: 'right',
        // lineHeight: 18,
        color: 'orange',

    },
    viewAddressText: {
        flexDirection: 'row',
        // marginBottom: 3,
        // width: 150,
    },
    textAddress: {
        fontStyle: "normal",
        fontWeight: 'bold'
    },
    imageRoom: {
        alignSelf: 'flex-end',
        width: 100,
        height: 80,
        borderRadius: 8.5
    },
    viewSubBottom: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textSubBottomProperty: {
        marginLeft: 5,
        fontSize: 11,
        color: '#474D60'
    },
});