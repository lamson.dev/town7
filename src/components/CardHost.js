import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";

export class CardType1 extends Component {
    render() {
        const { hostName, imgUri, price, starVoting, reviewTotal, onPress } = this.props;
        return (
            // <View>
            //     <Text style={{color: 'yellow'}}>card type 2 ★★★☆☆</Text>
            // </View>
            <View style={styles.viewCardType2} >
                <TouchableOpacity onPress={onPress}>
                    <View>

                        {/* image of card */}
                        <View>
                            <Image style={styles.imageCardType2} source={{ uri: imgUri }} />
                            <View style={styles.viewPrice}>
                                <Text style={styles.textPrice}>VND {price}</Text>
                            </View>
                        </View>
                        {/* Title of card */}
                        <Text style={styles.textTitleCard}>
                            {hostName}
                        </Text>

                        <View style={styles.viewStarReview}>
                            {/* Number of stars */}
                            <Text style={styles.textNumStar}>{starVoting} ★</Text>

                            {/* Number of total review */}
                            <Text style={styles.textNumReview}>{reviewTotal} Review</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View >
        )
    }
}

export class CardType2 extends Component {
    render() {
        const { hostName, imgUri, price, starVoting, totalReview, onPress } = this.props;
        return (
            <View style={styles.viewCard} >
                <TouchableOpacity onPress={onPress}>
                    <View>

                        {/* image of card */}
                        <View>
                            <Image style={styles.imageCard} source={{ uri: imgUri }} />
                            <View style={styles.viewPrice}>
                                <Text style={styles.textPrice}>VND {price}</Text>
                            </View>
                        </View>
                        {/* Title of card */}
                        <Text style={styles.textTitleCard}>
                            {hostName}
                        </Text>

                        <View style={styles.viewStarReview}>
                            {/* Number of stars */}
                            <Text style={styles.textNumStar}>{starVoting} ★</Text>

                            {/* Number of total review */}
                            <Text style={styles.textNumReview}>{totalReview} Review</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View >
        )
    }
}

export class CardType3 extends Component {
    render() {
        const { destination, imgUri, onPress } = this.props;
        return (
            <View style={styles.viewCardType3} >
                <TouchableOpacity onPress={onPress}>
                    <View>

                        {/* image of card */}
                        <View>
                            <Image style={styles.imageCardType3} source={{ uri: imgUri }} />

                            {/* Title of card */}
                            <Text style={styles.textTitleCardType3}>
                                {destination}
                            </Text>

                        </View>

                    </View>
                </TouchableOpacity>
            </View >
        )
    }
}
const styles = StyleSheet.create({
    viewListCard: {
        // backgroundColor: '#d5dceb'
    },
    viewRowListCard: {
        flexDirection: 'row'
    },
    viewCard: {
        backgroundColor: 'white',
        width: 150,
        marginRight: 10
    },
    viewCardType2: {
        backgroundColor: 'white',
        width: 240,
        marginRight: 10
    },
    viewCardType3: {
        backgroundColor: 'white',
        width: 185,
        height: 248,
        marginRight: 10
    },
    viewImageCard: {

    },
    imageCard: {
        borderRadius: 6,
        width: "100%",
        height: 100,
    },
    imageCardType2: {
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        width: "100%",
        height: 140,
    },
    imageCardType3: {
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        width: "100%",
        height: "100%",
        opacity: 0.85,
        zIndex: -1
    },
    viewPrice: {
        height: 25,
        position: 'absolute',
        bottom: 0,
        left: 12,
        backgroundColor: '#ff8519',
        justifyContent: 'center',
        borderTopLeftRadius: 7.5,
        borderTopRightRadius: 7.5,
        // opacity: 0.5
    },
    textPrice: {
        color: '#ffffff',
        fontStyle: 'normal',
        fontWeight: "600",
        fontSize: 16,
        lineHeight: 19,
        marginHorizontal: 12
    },

    textTitleCard: {
        marginTop: 8,
        marginBottom: 10,
        fontStyle: 'normal',
        fontWeight: "bold",
        fontSize: 14,
        lineHeight: 17,
        color: '#000000',
    },
    textTitleCardType3: {
        position: 'absolute',
        fontStyle: 'normal',
        fontWeight: "bold",
        fontSize: 20,
        lineHeight: 24,
        color: '#ffffff',
        bottom: 38,
        left: 27,
        textShadowColor: '#000',
        textShadowOffset: {
            width: 0,
            height: 4
        },
        textShadowRadius: 3.84,
        // shadowOpacity: 11.14
    },
    viewStarReview: {
        flexDirection: 'row'
    },
    textNumStar: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#1EA896',
        marginRight: 2.5
    },
    textNumReview: {
        marginLeft: 2.5,
        fontStyle: "italic",
        fontSize: 13.5,
        color: '#474D60',
    },
});
