import React, { Component } from "react";
import { View, Text, StyleSheet, } from "react-native";

export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }



    renderStar = (numOfStar) => {
        let star = [];

        for (let index = 0; index < 5; index++) {

            if (index < numOfStar) {
                star.push(<Text key={index} style={styles.textStar}>★</Text>);
            } else {
                star.push(<Text key={index} style={styles.textStar}>☆</Text>);
            }
        };
        return star;
    }

    render() {
        const { title, stars, name, dateReviews, content } = this.props;
        return (

            <View style={styles.viewCardReview}>
                <View style={styles.viewHeaderCardReview}>

                    {/* left */}
                    <View>
                        <Text style={styles.textTitleReview}>
                            {name}
                        </Text>
                        <View style={{ flexDirection: 'row' }}>
                            {this.renderStar(stars)}
                        </View>
                    </View>

                    {/* Right */}
                    <View>
                        <Text style={styles.textDateReview}>
                            {dateReviews}
                        </Text>
                    </View>
                </View>

                <Text style={styles.textReviewContent}>
                    {content}
                </Text>
            </View>

        )
    }
}

const styles = StyleSheet.create({

    viewCardReview: {
        minHeight: 80,
        backgroundColor: '#fff',
        borderRadius: 5,
        marginBottom: 7.5,
        paddingHorizontal: 4.5,
        paddingTop: 5,
        paddingBottom: 5,
        borderRadius: 8.5,
    },
    viewHeaderCardReview: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textTitleReview: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#24253D',
    },
    textNameReviewer: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 13,
        color: '#24253D',
    },
    textDateReview: {
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        color: '#91929e'
    },
    textReviewContent: {
        marginTop: 5,
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 15,
        lineHeight: 22,
        color: '#39393A'
    },
    textStar:{
        color: 'orange'
    }
});