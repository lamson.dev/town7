import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

import { Calendar } from 'react-native-calendars';

const nowDate = new Date();
export default class CalendarModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentDate: nowDate.getFullYear() + '-' + ((nowDate.getMonth() + 1) < 10 ? '0' + (nowDate.getMonth() + 1) : (nowDate.getMonth() + 1)) + '-' + (nowDate.getDate() > 9 ? nowDate.getDate() : "0" + nowDate.getDate()),

            checkType: props.checkType,

            checkinDate: props.checkinDate,
            checkoutDate: props.checkoutDate,

            bookingDateOnCalendar: {
                imposibleBookingDate: {},
            },

            imposibleBookingDate: {},
        }

    }

    converDateFormat(date) {
        var tempDate = new Date(date);
        return nowDate.getFullYear() + '-' + ((tempDate.getMonth() + 1) < 10 ? '0' + (tempDate.getMonth() + 1) : (tempDate.getMonth() + 1)) + '-' + (tempDate.getDate() > 9 ? tempDate.getDate() : "0" + tempDate.getDate())
    }

    // callback
    setDateSeleted = (date) => {

        // check valid check in
        var tempDate = new Date(date);
        var tempListImpossible = Object.keys(this.state.imposibleBookingDate);
        let statusCheck = true;

        this.state.checkType === 'checkin' ? tempDate.setDate(tempDate.getDate() + 1) : tempDate.setDate(tempDate.getDate() - 1);

        if (tempListImpossible.includes(this.converDateFormat(tempDate))) {
            alert('This date only for returning accommodation, please select another date.');
        } else {

            // check booking date duplication with imposible date booking
            if (this.state.checkType === 'checkin') {

                if (this.state.checkoutDate) {
                    // checkin_input > checkout
                    if (new Date(date) >= new Date(this.state.checkoutDate)) {
                        alert('checkin date invalid');
                    } else {
                        // check checkin date input if overwrite on other booking
                        for (let index = new Date(date); index < new Date(this.state.checkoutDate); index.setDate(index.getDate() + 1)) {
                            if (tempListImpossible.includes(this.converDateFormat(index))) {
                                statusCheck = false;
                                break;
                            }
                        }

                        if (statusCheck) {
                            this.props.getDateSeletedCallback(date);
                            this.props.closeCalendar();
                        } else {
                            alert('Checkin is duplicated, please select another date.');
                        }
                    }

                } else {
                    this.props.getDateSeletedCallback(date);
                    this.props.closeCalendar();
                }

            } else {

                if (this.state.checkinDate) {

                    if (new Date(date) <= new Date(this.state.checkinDate)) {
                        alert('checkout date invalid');
                    } else {
                        for (let index = new Date(date); index > new Date(this.state.checkinDate); index.setDate(index.getDate() - 1)) {
                            if (tempListImpossible.includes(this.converDateFormat(index))) {
                                statusCheck = false;
                                break;
                            }
                        }

                        if (statusCheck) {
                            this.props.getDateSeletedCallback(date);
                            this.props.closeCalendar();
                        } else {
                            alert('Checkout is duplicated, please select another date.');
                        }
                    }

                } else {
                    console.log('da chon checkout: ', date);
                    this.props.getDateSeletedCallback(date);
                    this.props.closeCalendar();
                }
            }


            // this.props.closeCalendar();
        }
    }

    createImposibleBookingDates = (from, to) => {
        var fromDate = new Date(from);
        var toDate = new Date(to);
        var listDateImposible = {};

        for (let index = fromDate; index <= toDate; index.setDate(index.getDate() + 1)) {
            listDateImposible[this.converDateFormat(index)] = {
                disableTouchEvent: true,
                disabled: true,
                color: '#fc502d',
                // startingDay: true,
                // endingDay: true
            }
        };

        return listDateImposible;
    }

    componentDidMount() {
        //    create imposible list object date
        // var tempArrDate = [

        //     {
        //         from: "2020-09-17",
        //         to: "2020-09-18"
        //     },
        //     {
        //         from: "2020-09-22",
        //         to: "2020-09-24"
        //     },
        //     {
        //         from: "2020-10-04",
        //         to: "2020-10-06"
        //     },
        //     {
        //         from: "2020-10-14",
        //         to: "2020-10-18"
        //     },
        // ]

        var listDateImposible = {};
        if (this.props.imposibleBookingDate !== undefined) {
            console.log('props cancel date: ', this.props.imposibleBookingDate);
            this.props.imposibleBookingDate.forEach(el => {
                Object.assign(listDateImposible, this.createImposibleBookingDates(el.from, el.to));
            })

            console.log(listDateImposible);

            this.setState({
                imposibleBookingDate: listDateImposible
            });
        }

    }

    render() {
        return (

            <View style={styles.viewMain}>

                <View style={styles.viewCalendar}>

                    <View style={styles.viewCloseTouch}>
                        <TouchableOpacity style={styles.touchClose}
                            onPress={this.props.closeCalendar}>
                            <Text style={{ fontSize: 27 }}>✖</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{this.state.checkType}</Text>
                    <Calendar
                        // Initially visible month. Default = Date()
                        // current={this.state.currentDate}
                        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                        minDate={this.state.currentDate}
                        // minDate={'2020-04-12'}
                        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                        // maxDate={'2012-05-30'}
                        // Handler which gets executed on day press. Default = undefined
                        // onDayPress={(day) => { this.setDateSeleted(this.state.dateStartSeleted == null ? 'start' : 'end', day.dateString) }}

                        onDayPress={(day) => { this.setDateSeleted(day.dateString) }}

                        // Handler which gets executed on day long press. Default = undefinedzbu
                        // onDayLongPress={(day) => { console.log('selected day-long', day) }}
                        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                        monthFormat={'yyyy MM'}
                        // Handler which gets executed when visible month changes in calendar. Default = undefined
                        onMonthChange={(month) => { console.log('month changed', month) }}
                        // Hide month navigation arrows. Default = false
                        hideArrows={false}
                        // Replace default arrows with custom ones (direction can be 'left' or 'right')
                        // renderArrow={(direction) => (<Arrow />)}
                        // Do not show days of other months in month page. Default = false
                        hideExtraDays={true}
                        // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                        // day from another month that is visible in calendar page. Default = false
                        disableMonthChange={true}
                        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                        firstDay={1}
                        // Hide day names. Default = false
                        // hideDayNames={true}
                        // Show week numbers to the left. Default = false
                        showWeekNumbers={false}
                        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                        onPressArrowLeft={substractMonth => substractMonth()}
                        // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                        onPressArrowRight={addMonth => addMonth()}
                        // Disable left arrow. Default = false
                        disableArrowLeft={false}
                        // Disable right arrow. Default = false
                        disableArrowRight={false}
                        markedDates={
                            this.state.imposibleBookingDate
                            // {
                            //     "2020-09-26":{
                            //         startingDay: true,
                            //         endingDay: true,
                            //         selected: true,
                            //         // disableTouchEvent: true,
                            //         // color: '#f76e19',
                            //         color: '#24c3f0'
                            //     }
                            // }
                            // {

                            //     [this.state.dateSeleted ? this.state.dateSeleted : this.state.currentDate]: {
                            //         startingDay: true,
                            //         endingDay: true,
                            //         selected: true,
                            //         disableTouchEvent: true,
                            //         color: '#f76e19',
                            //         color: '#24c3f0',
                            //         disabled: true,
                            //         disableTouchEvent: true
                            //     },

                            // [this.state.testDate]: {
                            //     disableTouchEvent: true,
                            //     disabled: true, startingDay: true, color: 'green', endingDay: true
                            // },

                            // }
                        }
                        markingType={'period'}
                        theme={{
                            monthTextColor: '#24c3f0',
                        }}
                    />
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        flexWrap: "nowrap",
        justifyContent: 'center',
        // opacity: 0,
        // backgroundColor: 'green'
        backgroundColor: 'rgba(0,0,0,.65)',
    },
    viewCalendar: {
        backgroundColor: '#fff',
        paddingBottom: 10,
        borderRadius: 15
    },
    viewCloseTouch: {
        // backgroundColor: '#383bff',
        width: 30,
        height: 30,
        marginRight: 10,
        justifyContent: 'center',
        alignSelf: 'flex-end',
        alignItems: 'center',
        borderWidth: 0
    },
    toucClose: {
        flex: 1,
        justifyContent: 'center',
    }
});