import { takeLatest } from "redux-saga/effects";

import { REGISTER_USER_REQUEST, AUTH_USER_REQUEST, LOGOUT_REQUEST } from "../actions/types";
import { registerSaga, loginSaga, logoutSaga } from "./auth.saga";

export default function* watcherUserAuthentication() {
    console.log('-authenticating watcher act');
    yield takeLatest(REGISTER_USER_REQUEST, registerSaga);
    yield takeLatest(AUTH_USER_REQUEST, loginSaga);
    yield takeLatest(LOGOUT_REQUEST, logoutSaga);
}