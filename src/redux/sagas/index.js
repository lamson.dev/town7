import { fork,all } from "redux-saga/effects";

import watcherUserAuthentication from "./watchers";

export default function* rootSaga() {
    // yield [
    //     fork(watcherUserAuthentication)
    // ];
    yield fork(watcherUserAuthentication);
}