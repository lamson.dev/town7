import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

import { Calendar } from 'react-native-calendars';

const nowDate = new Date();
export default class Test extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentDate: nowDate.getFullYear() + '-' + ((nowDate.getMonth() + 1) < 10 ? '0' + (nowDate.getMonth() + 1) : (nowDate.getMonth() + 1)) + '-' + (nowDate.getDate() > 9 ? nowDate.getDate() : "0" + nowDate.getDate()),

            bookingDateOnCalendar: {
                imposibleBookingDate: {},
            },

            imposibleBookingDate: {},

            testHehe: {
                "2020-09-23": {
                    disableTouchEvent: true,
                    disabled: true,
                    startingDay: true,
                    color: 'red',
                    endingDay: true
                },
                "2020-09-24": {
                    disableTouchEvent: true,
                    disabled: true,
                    startingDay: true,
                    color: 'red',
                    endingDay: true
                },
                "2020-09-25": {
                    disableTouchEvent: true,
                    disabled: true,
                    startingDay: true,
                    color: 'red',
                    endingDay: true
                },
            },
            // dateStartSeleted: null,
            // dateEndSeleted: null,

        }

    }

    converDateFormat(date) {
        var tempDate = new Date(date);
        return nowDate.getFullYear() + '-' + ((tempDate.getMonth() + 1) < 10 ? '0' + (tempDate.getMonth() + 1) : (tempDate.getMonth() + 1)) + '-' + (tempDate.getDate() > 9 ? tempDate.getDate() : "0" + tempDate.getDate())
    }

    // callback
    setDateSeleted = (date, type = 'checkout') => {

        // check valid check in
        var tempDate = new Date(date);
        var tempListPossible = Object.keys(this.state.imposibleBookingDate);

        type === 'checkin' ? tempDate.setDate(tempDate.getDate() + 1) : tempDate.setDate(tempDate.getDate() - 1);

        if (tempListPossible.includes(this.converDateFormat(tempDate))) {
            alert('Ngày không hợp lệ, vui lòng chọn ngày khác.');
        }

        console.log('set start is seleted: ', this.converDateFormat(tempDate));

        // this.props.getDateSeletedCallback(date);
    }

    createImposibleBookingDates = (from, to) => {
        var fromDate = new Date(from);
        var toDate = new Date(to);
        var listDateImposible = {};

        for (let index = fromDate; index <= toDate; index.setDate(index.getDate() + 1)) {
            listDateImposible[this.converDateFormat(index)] = {
                disableTouchEvent: true,
                disabled: true,
                startingDay: true,
                color: 'red',
                endingDay: true
            }
        };

        return listDateImposible;
    }

    componentDidMount() {
        //    create imposible list object date
        var tempArrDate = [

            {
                from: "2020-09-16",
                to: "2020-09-18"
            },
            {
                from: "2020-09-22",
                to: "2020-09-24"
            },
        ]

        var listDateImposible = {};

        tempArrDate.forEach(el => {
            Object.assign(listDateImposible, this.createImposibleBookingDates(el.from, el.to));
        })

        this.setState({
            imposibleBookingDate: listDateImposible
        });


        // result.setDate(result.getDate() + 2);
    }

    render() {
        return (

            <View style={styles.viewMain}>

                <View style={styles.viewCalendar}>

                    <View style={styles.viewCloseTouch}>
                        <TouchableOpacity style={styles.touchClose}
                            onPress={this.props.closeCalendar}>
                            <Text style={{ fontSize: 27 }}>✖</Text>
                        </TouchableOpacity>
                    </View>
                    <Calendar
                        // Initially visible month. Default = Date()
                        // current={this.state.currentDate}
                        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                        minDate={this.state.currentDate}
                        // minDate={'2020-04-12'}
                        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                        // maxDate={'2012-05-30'}
                        // Handler which gets executed on day press. Default = undefined
                        // onDayPress={(day) => { this.setDateSeleted(this.state.dateStartSeleted == null ? 'start' : 'end', day.dateString) }}

                        onDayPress={(day) => { this.setDateSeleted(day.dateString) }}

                        // Handler which gets executed on day long press. Default = undefinedzbu
                        // onDayLongPress={(day) => { console.log('selected day-long', day) }}
                        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                        monthFormat={'yyyy MM'}
                        // Handler which gets executed when visible month changes in calendar. Default = undefined
                        onMonthChange={(month) => { console.log('month changed', month) }}
                        // Hide month navigation arrows. Default = false
                        hideArrows={false}
                        // Replace default arrows with custom ones (direction can be 'left' or 'right')
                        // renderArrow={(direction) => (<Arrow />)}
                        // Do not show days of other months in month page. Default = false
                        hideExtraDays={true}
                        // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                        // day from another month that is visible in calendar page. Default = false
                        disableMonthChange={true}
                        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                        firstDay={1}
                        // Hide day names. Default = false
                        // hideDayNames={true}
                        // Show week numbers to the left. Default = false
                        showWeekNumbers={false}
                        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                        onPressArrowLeft={substractMonth => substractMonth()}
                        // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                        onPressArrowRight={addMonth => addMonth()}
                        // Disable left arrow. Default = false
                        disableArrowLeft={false}
                        // Disable right arrow. Default = false
                        disableArrowRight={false}
                        markedDates={
                            this.state.imposibleBookingDate
                            // {
                            //     "2020-09-26":{
                            //         startingDay: true,
                            //         endingDay: true,
                            //         selected: true,
                            //         // disableTouchEvent: true,
                            //         // color: '#f76e19',
                            //         color: '#24c3f0'
                            //     }
                            // }
                            // {

                            //     [this.state.dateSeleted ? this.state.dateSeleted : this.state.currentDate]: {
                            //         startingDay: true,
                            //         endingDay: true,
                            //         selected: true,
                            //         disableTouchEvent: true,
                            //         color: '#f76e19',
                            //         color: '#24c3f0',
                            //         disabled: true,
                            //         disableTouchEvent: true
                            //     },

                            // [this.state.testDate]: {
                            //     disableTouchEvent: true,
                            //     disabled: true, startingDay: true, color: 'green', endingDay: true
                            // },

                            // }
                        }
                        markingType={'period'}
                        theme={{
                            monthTextColor: '#24c3f0',
                        }}
                    />
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        flexWrap: "nowrap",
        justifyContent: 'center',
        // opacity: 0,
        // backgroundColor: 'green'
        backgroundColor: 'rgba(0,0,0,.65)',
    },
    viewCalendar: {
        backgroundColor: '#fff',
        paddingBottom: 10,
        borderRadius: 15
    },
    viewCloseTouch: {
        // backgroundColor: '#383bff',
        width: 30,
        height: 30,
        marginRight: 10,
        justifyContent: 'center',
        alignSelf: 'flex-end',
        alignItems: 'center',
        borderWidth: 0
    },
    toucClose: {
        flex: 1,
        justifyContent: 'center',
    }
});