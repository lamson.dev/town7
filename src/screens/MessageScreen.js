import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";

export default class MessageScreen extends Component {
    render() {
        return (
            <View style={styles.mainView}>
                <Text style={styles.textLogo}>
                    Message Screen
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // backgroundColor: '#69a6eee3',
        justifyContent: 'center',
        alignItems: 'center'        
    },
});