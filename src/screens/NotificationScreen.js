import React, { Component } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import CardNotification from "../components/CardNotification";
import NotificationService from "../services/notification.service";
import LoaderModal from "../components/LoaderModal";
import { Navigation } from "react-native-navigation";

export default class NotificationScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listNotification: [],
            totalNotificationUnRead: 0,
            loading: false
        };
    }

    async componentDidMount() {
        await this.fetchNotification();
    }

    fetchNotification = async () => {
        this.setState({ loading: true });

        await NotificationService.getAll()
            .then(response => {
                this.setState({
                    loading: false,
                    listNotification: response.data.data,
                    totalNotificationUnRead: response.data.data != null ? response.data.data.filter(value => value.read === false).length : 0
                })
            }).catch(error => {
                this.setState({ loading: false });
                alert('Error: ' + error);
            });

        Navigation.mergeOptions("notificationTab", {
            bottomTab: {
                badge: this.state.totalNotificationUnRead < 1 ? null : this.state.totalNotificationUnRead,
            }
        });
        this.setState({ loading: false });

    }

    readNotification = async (id) => {

        await NotificationService.read(id)
            .then((response) => {
                this.fetchNotification();
            }).catch(error => {
                alert('Error: ' + error)
            })
    }

    renderCardBookingRoom = (notifications) => {
        return (
            <FlatList
                horizontal={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={notifications}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardNotification
                            onPress={() => this.readNotification(item.id)}
                            title={item.title}
                            content={item.content}
                            time={item.createdAt}
                            status={item.read}
                        />
                }
            />
        )
    }

    render() {
        return (
            <View style={styles.mainView}>
                <LoaderModal
                    loading={this.state.loading} />
                {/* main area notification */}
                <View>
                    {this.renderCardBookingRoom(this.state.listNotification)}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
});