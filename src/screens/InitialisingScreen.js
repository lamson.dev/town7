import React from 'react';
import { View, StyleSheet, ActivityIndicator, Image, } from 'react-native';

import { goToLogin, showSideTabsMenu } from '../navigator/navigation';
import { fetchUserDataService } from "../services/fetchUserData.service";

class InitialisingScreen extends React.Component {

    constructor(props) {
        super(props);
    }
    static options() {
        return {
            statusBar: {
                backgroundColor: '#3389EE',
                style: "light"
            }
        };

    }

    async componentDidMount() {

        await fetchUserDataService().then((response) => {
            showSideTabsMenu(response.data.data)
        }).catch(error => {
            console.log(error);
            goToLogin()
        });

    }

    render() {
        const { isLoggedIn } = this.props;
        return (
            <View style={styles.container}>
                <Image style={styles.imgLogo} source={require('../assets/images/Logo-app.png')}></Image>
                <ActivityIndicator size="large" color="#fff" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    textWelcome: {
        fontSize: 28,
        color: '#fff'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3389EE'
    },
    viewLogo: {
        marginVertical: 5
    },
    imgLogo: {
        width: 285,
        height: 285,
        alignSelf: 'center',
    },
})

export default InitialisingScreen;