import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, Modal } from "react-native";
import { Navigation } from "react-native-navigation";

import { mStyles } from "../styles/styles";
import ButtonMain from "../components/ButtonMain";
import LoaderModal from "../components/LoaderModal";
import CalendarModal from "../components/CalendarModal";
import GuestQueryModal from "../components/GuestQueryModal";
import CheckOutScreen from "./CheckoutScreen";

import { showAlertFeatureIncomplete } from "../utils/alert.utils";
import { calculateBookingPriceService } from "../services/calculateBookingPrice.service";
import RoomService from "../services/room.service";

export default class CalculatePriceBookingScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            roomData: props.roomDataProps,

            imposibleBookingDate: [

                {
                    from: "2020-09-21",
                    to: "2020-09-24"
                },

            ],

            modalCalendarVisible: false,
            dateInOutSeleted: true,

            checkType: 'checkin',//default type
            checkinDate: null,
            checkoutDate: null,

            guestQuery: {
                adult: 1,
                children: 0,
                infants: 0
            },
            totalGuest: 1,
            loading: false,
        };
    }

    openGuestQueryModal = () => {
        if (this.guestQueryModal) {
            this.guestQueryModal.togleModal();
        }
    }

    openCalendarModal(type = 'checkin') {

        if (type == 'checkin') {
            // seleteing checkin date
            this.setState({
                dateInOutSeleted: true,
                modalCalendarVisible: true,
                checkType: 'checkin',
            });
        } else {
            // select checkout date
            this.setState({
                dateInOutSeleted: false,
                modalCalendarVisible: true,
                checkType: 'checkout',

            });
        }

    }

    closeCalendarModal = () => {
        this.setState({ modalCalendarVisible: false });
    }

    setDateSeleted = (date) => {
        // console.log('date test: ', date);
        // console.log('dateinout: ', this.state.dateInOutSeleted);
        this.state.dateInOutSeleted ?
            this.setState({ checkinDate: date }) : this.setState({ checkoutDate: date });

        // this.closeCalendarModal();
    }

    doneGuestQuery = (guestQueryData) => {
        this.setState({
            guestQuery: {
                adult: guestQueryData.adults,
                children: guestQueryData.children,
                infants: guestQueryData.infants
            },
            totalGuest: (guestQueryData.adults + guestQueryData.children + guestQueryData.infants)
        })
    }

    gotoCheckout = (bookingData) => {
        Navigation.registerComponent('CheckOutScreen', () => CheckOutScreen)

        Navigation.push(this.props.componentId, {
            component: {
                name: 'CheckOutScreen',
                passProps: {
                    bookingDataProps: bookingData
                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        title: {
                            text: 'Checkout'
                        },
                        backButton: {
                            color: '#3389EE',
                        },
                        // drawBehind: true,
                        background: {
                            color: '#F2F2F2'
                        },
                        noBorder: true,
                        elevation: 0,
                    }
                }
            }
        });
    }

    nextStep = async () => {
        const bookingData = {
            hostId: this.state.roomData.id,
            numberOfAdultGuest: this.state.guestQuery.adult,
            numberOfChildrenGuest: this.state.guestQuery.children,
            numberOfInfantGuest: this.state.guestQuery.infants,
            startDate: this.state.checkinDate,
            endDate: this.state.checkoutDate
        }
        if (bookingData.startDate == null || bookingData.endDate == null) {
            alert('Please fill data');
            return null;
        }

        this.setState({ loading: true });
        console.log('calculate data: ', bookingData)
        await calculateBookingPriceService(bookingData).then((response) => {
            this.setState({ loading: false });
            if (response.data.data != null) {
                this.gotoCheckout(response.data.data);
            } else {
                alert('Can not handle request');
            }
        }).catch(error => {
            this.setState({ loading: false });
            alert('Can not handle request:', error);
        });
    }

    async componentDidMount() {
        await RoomService.getDateLook(this.state.roomData.id).then(response => {
            console.log('date look: ', response);
            this.setState({ imposibleBookingDate: response.data.data.dates })
        }).catch(error => {
            alert(error);
        })
    }

    render() {

        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <LoaderModal
                    loading={this.state.loading} />
                <ScrollView>

                    <View style={styles.viewRoomInfo}>

                        <View>
                            <Text style={styles.textRoomType}>
                                {this.state.roomData.hostCategory.name}
                            </Text>
                            <Text style={styles.textPrice}>VND {this.state.roomData.standardPriceMondayToThursday} / night</Text>
                        </View>
                        <TouchableOpacity>
                            <Image style={styles.imageRoom} source={{ uri: "https://ezcloud.vn/wp-content/uploads/2019/03/kinh-doanh-homestay-kiem-tien-trieu.jpg" }} />
                        </TouchableOpacity>
                    </View>

                    {/* Calendar Modal, CheckIn, CheckOut, Number Room */}
                    <View style={styles.viewCheckInOutRoom}>

                        {/* Modal: Calendar, GuestQuery */}
                        <View>
                            <Modal
                                visible={this.state.modalCalendarVisible}
                                animationType={'slide'}
                                transparent>
                                <CalendarModal
                                    imposibleBookingDate={this.state.imposibleBookingDate}
                                    checkType={this.state.checkType}
                                    checkinDate={this.state.checkinDate}
                                    checkoutDate={this.state.checkoutDate}
                                    closeCalendar={this.closeCalendarModal}
                                    getDateSeletedCallback={this.setDateSeleted} />
                            </Modal>
                            <GuestQueryModal
                                ref={ref => {
                                    this.guestQueryModal = ref;
                                }}
                                getGuestQuery={this.doneGuestQuery}
                                maxAdult={this.state.roomData.numberOfAdultGuest}
                                maxChildren={this.state.roomData.numberOfChildrenGuest} />
                        </View>


                        {/* Checkin Touch */}
                        <TouchableOpacity
                            style={styles.touchProperty}
                            onPress={() => this.openCalendarModal()}>
                            <View>
                                <Text style={styles.textTouchLabel}>Check-In</Text>
                                <Text style={styles.textTouchProperty}>
                                    {this.state.checkinDate ? this.state.checkinDate : "---- --  --"}
                                </Text>
                            </View>
                        </TouchableOpacity>

                        {/* Checkout Touch */}
                        <TouchableOpacity
                            style={styles.touchProperty}
                            onPress={() => this.openCalendarModal('checkout')}>
                            <View>
                                <Text style={styles.textTouchLabel}>Check-Out</Text>
                                <Text style={styles.textTouchProperty}>
                                    {this.state.checkoutDate ? this.state.checkoutDate : "---- --  --"}
                                </Text>
                            </View>
                        </TouchableOpacity>

                        {/* Number of Guest */}
                        <TouchableOpacity
                            style={styles.touchProperty}
                            onPress={this.openGuestQueryModal}>
                            <View>
                                <Text style={styles.textTouchLabel}>Guest</Text>
                                <Text style={styles.textTouchProperty}>
                                    {this.state.totalGuest}
                                </Text>
                            </View>
                        </TouchableOpacity>

                    </View>




                </ScrollView>

                <View style={styles.viewButtonBook}>
                    <ButtonMain
                        nameBtn={'CONTINUE'}
                        onPress={this.nextStep} />
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
    },
    viewRoomInfo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 20,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#fff'
    },
    textRoomType: {
        fontSize: 14.5,
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: '#c4194a',
        letterSpacing: 2,
        textTransform: 'uppercase'
    },
    textPrice: {
        fontSize: 20,
        fontStyle: 'normal',
        lineHeight: 24,
        color: '#282a2e',
        marginTop: 10
    },
    imageRoom: {
        width: 110,
        height: 80,
    },

    viewCheckInOutRoom: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop: 20,
        marginHorizontal: 20,
        // backgroundColor: '#FFFF',
        marginBottom: 15,
    },
    touchProperty: {
        marginBottom: 5,
        width: 100,
        height: 60,
        justifyContent: 'center',
        borderWidth: 0.7,
        borderRadius: 8,
        borderColor: '#fff',
        backgroundColor: '#FFFF',
    },
    textTouchLabel: {
        fontWeight: '600',
        fontSize: 10.5,
        textAlign: 'center',
        textTransform: 'uppercase',
        color: '#91929e',
    },
    textTouchProperty: {
        textAlign: 'center',
        fontWeight: '200',
        fontSize: 17,
        lineHeight: 20,
        color: '#1e1e1f',
    },

    viewFee: {
        // backgroundColor: 'blue',
        marginTop: 10,
        marginHorizontal: 20,

        paddingTop: 15,
        borderTopWidth: 1,
        borderTopColor: '#fff',

        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#fff'

    },
    textFeeTax: {
        fontWeight: 'bold',
        color: '#1e1e1f',
        fontSize: 13
    },
    viewFeeTax: {
        marginVertical: 5
    },
    viewFeeTaxSub: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'blue',
        marginVertical: 5
    },

    viewTotalFee: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        marginTop: 5
    },
    textTotalFee: {
        fontSize: 14.5,
    },
    viewButtonBook: {
        paddingBottom: 7,
        marginHorizontal: 19,
    }
});