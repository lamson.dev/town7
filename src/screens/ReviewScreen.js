import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

import { mStyles } from "../styles/styles";
import CardReviews from "../components/CardReviews";
import ReviewService from "../services/review.service";
import LoaderModal from "../components/LoaderModal";

export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hostId: props.hostId,
            starsNumber: {
                oneStar: 0,
                twoStar: 0,
                threeStar: 0,
                fourStar: 0,
                fiveStar: 0
            },
            listReviews: [],
            loading: false,
            avgStar: 0,
        };
    }

    renderRowStars = (oneStar = 0, twoStar = 0, threeStar = 0, fourStar = 0, fiveStar = 0) => {

        var starArr = [];
        var starElements = [];

        var votingNumStar = [oneStar, twoStar, threeStar, fourStar, fiveStar];
        var sumVoting = votingNumStar.reduce((a, b) => a + b, 0);

        for (let i = 5; i > 0; i--) {

            for (let j = i; j > 0; j--) {
                starElements.push('✩');
            }

            starArr.push(
                <View style={{ flexDirection: 'row', width: '100%' }} key={i}>
                    {/* stars */}
                    <Text style={{ fontSize: 7, width: '20%', textAlign: 'right', marginRight: 2.5 }}>
                        {starElements}
                    </Text>
                    {/* progress bar */}
                    <View style={{ justifyContent: 'center', width: '80%', marginLeft: 2.5 }}>
                        <View style={{ height: 3.5, width: '100%', backgroundColor: '#fff' }}>
                            <View style={{ height: 3.5, width: (votingNumStar[i - 1] / (sumVoting) * 100) + '%', backgroundColor: '#3389EE' }} />
                        </View>
                    </View>
                </View>
            );
            starElements = [];
        }

        return starArr;
    }

    renderReviews = (reviews) => {
        return (
            <FlatList
                // contentContainerStyle={{paddingBottom: 90}}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={reviews}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardReviews
                            stars={item.startsRating}
                            name={item.fullName}
                            dateReview={item.createAt}
                            content={item.content} />
                }
            />
        )
    }

    async componentDidMount() {
        this.setState({ loading: true });
        await ReviewService.getById(this.state.hostId).then(response => {
            console.log(response);
            this.setState({
                loading: false,
                listReviews: response.data.data.listReview,
                avgStar: Math.round(response.data.data.stars * 10) / 10
            });
        }).catch(error => {
            this.setState({ loading: false });
            alert('Error: ' + error)
        });

        let tempStarsNumber = {
            oneStar: 0,
            twoStar: 0,
            threeStar: 0,
            fourStar: 0,
            fiveStar: 0
        };

        this.state.listReviews.forEach(el => {
            el.startsRating == 1 ? tempStarsNumber.oneStar += 1 : null;
            el.startsRating == 2 ? tempStarsNumber.twoStar += 1 : null;
            el.startsRating == 3 ? tempStarsNumber.threeStar += 1 : null;
            el.startsRating == 4 ? tempStarsNumber.fourStar += 1 : null;
            el.startsRating == 5 ? tempStarsNumber.fiveStar += 1 : null;
        });

        console.log(tempStarsNumber);
        this.setState({
            loading: false,
            starsNumber: tempStarsNumber
        });
    }

    render() {
        return (
            <View style={[styles.viewMain, mStyles.bkgColorMain]}>
                <LoaderModal
                    loading={this.state.loading} />
                {/* top view */}
                <View style={styles.viewTop}>

                    {/* total stars,  */}
                    <View style={styles.viewRowTop}>

                        {/* left */}
                        <View style={{ flex: 1 }}>

                            <Text style={styles.textAvgStar}>
                                {this.state.avgStar}
                            </Text>
                            <Text style={styles.textOutOf5}>
                                (OUT OF 5)
                            </Text>
                        </View>

                        {/* right */}
                        <View style={{ flex: 2, }}>

                            {/* stars matrix */}
                            <View style={{ width: '100%' }}>

                                {/* row star */}
                                {this.renderRowStars(
                                    this.state.starsNumber.oneStar,
                                    this.state.starsNumber.twoStar,
                                    this.state.starsNumber.threeStar,
                                    this.state.starsNumber.fourStar,
                                    this.state.starsNumber.fiveStar,
                                )}

                            </View>

                            {/* Toto rating(reviews) */}
                            <Text style={styles.textTotalReview}>
                                {this.state.listReviews.length < 1 ? 0 : this.state.listReviews.length} Ratings
                            </Text>
                        </View>
                    </View>
                </View>

                {/* body: card reviews */}

                <View style={styles.viewListReview}>

                    {this.renderReviews(this.state.listReviews)}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        paddingHorizontal: 20
    },
    viewRowTop: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'blue'
    },
    textReviewTitle: {
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 26,
        lineHeight: 32,
        color: '#24253D'
    },
    touchWriteReview: {
        justifyContent: 'center'
    },

    textAvgStar: {
        textAlign: 'center',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 70,
        lineHeight: 75,
        color: '#F79F27'
    },
    textOutOf5: {
        textAlign: 'center',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 16,
        color: '#91929e'
    },

    textTotalReview: {
        textAlign: 'right',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 13,
        color: '#24253D'
    },

    viewListReview: {
        marginTop: 10,
        flex: 1
    },

});