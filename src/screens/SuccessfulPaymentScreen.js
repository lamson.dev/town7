import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Navigation } from "react-native-navigation";

import ButtonMain from "../components/ButtonMain";
import { mStyles } from "../styles/styles";
import { showSideTabsMenu } from "../navigator/navigation";
export default class SuccessfulPaymentScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    onContinuePress = () => {
        // Navigation.dismissModal(this.props.componentId);
        showSideTabsMenu();
    }

    render() {
        return (
            <View style={[styles.viewMain]}>
                <View>
                    <Text style={[mStyles.textMainTitle, mStyles.mrgBottMainTitle]}>
                        Reservation completed successfully !
                    </Text>

                    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
                        <Text style={{ fontSize: 19.5, textAlign: 'center' }}>
                            Your reservation with an our partner was successful. Enjoy your trip
                        </Text>
                    </View>
                    <View style={mStyles.viewBtnMain}>
                        <ButtonMain nameBtn={'CONTINUE EXPORE'} onPress={this.onContinuePress} />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
        justifyContent: 'center'
    },

});