import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Keyboard } from "react-native";
import { Navigation } from "react-native-navigation";
import { Icon } from "react-native-elements";

import TextInputComp from "../components/TextInputComp";
import RoomService from "../services/room.service";
import DetailRoomScreen from "./DetailRoomScreen";
import { showAlertFeatureIncomplete } from "../utils/alert.utils";
import { mStyles } from "../styles/styles";
import CardHostList from "../components/CardHostList";
import LoaderModal from "../components/LoaderModal";

export default class SearchListViewScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchValue: '',
            premuimCheck: true,
            searchKeywords: props.keywordProps,
            hostResultData: [],
            loading: false
        };
    }

    gotoRoomDetail = (roomId) => {
        Navigation.registerComponent('DetailRoomScreen', () => DetailRoomScreen)
        Navigation.push(this.props.componentId, {
            component: {
                name: 'DetailRoomScreen',
                passProps: {
                    roomId: roomId
                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        backButton: {
                            color: '#ffff',
                        },
                        drawBehind: true,
                        background: {
                            color: 'transparent'
                        },
                        noBorder: true,
                        elevation: 0,
                    }
                }
            }
        });
    }

    searchDestinationHost = async () => {
        Keyboard.dismiss();

        this.setState({loading: true});
        await RoomService.searchRoom(this.state.searchKeywords).then((response) => {
            this.setState({
                hostResultData: response.data.data
            })
            this.setState({loading: false});
        }).catch(error => {
            this.setState({loading: false});
            console.log('error resutl: ', error);
            this.setState({
                hostResultData: null
            });
        })
        this.setState({loading: false});
    }

    renderCardResult = (host) => {
        return (
            <FlatList
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={host}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardHostList
                            onPress={() => this.gotoRoomDetail(item.id)}
                            hostName={item.name}
                            imgUri={"https://ezcloud.vn/wp-content/uploads/2019/03/kinh-doanh-homestay-kiem-tien-trieu.jpg"}
                            price={item.standardPriceMondayToThursday}
                            starVoting={item.stars ? item.stars : 0}
                            hostCity={item.cityName}
                            totalReview={item.totalReview ? item.totalReview : 0} />
                }
            />
        )
    }

    componentDidMount() {
        this.searchDestinationHost();
    }

    render() {
        console.log(this.state.hostResultData);
        return (
            <View style={[mStyles.bkgColorMain, styles.viewMain]}>
                <LoaderModal
                    loading={this.state.loading} />
                {/* Top View */}
                <View style={styles.viewTop}>
                    {/* search view */}
                    <TextInputComp
                        placeholder={'Where to?'}
                        buttonRight={true}
                        nameIconBtn={'search'}
                        typeIconBtn={'fontawesome5'}
                        defaultValue={this.state.searchKeywords}
                        onChangeText={(value) => this.setState({ searchKeywords: value })}
                        buttonRightPress={() => this.searchDestinationHost()}
                        onSubmitEditing={() => this.searchDestinationHost()} />

                   
                </View>

                {/* List view of result */}
                <View style={styles.viewListResult}>
                    {/* card view */}
                    {this.state.hostResultData ? this.renderCardResult(this.state.hostResultData) : <Text>not found any result</Text>}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
    },
    viewTop: {
        width: "100%",
        marginBottom: 3.5,
        backgroundColor: '#24c3f0',
        paddingTop: 30
    },

    viewCheckInOutRoom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 2.5,
        marginHorizontal: 20
    },
    touchProperty: {
        marginBottom: 5,
        paddingLeft: 9,
        width: 112,
        paddingVertical: 5,
        justifyContent: 'center',
        borderWidth: 0.7,
        borderRadius: 8,
        borderColor: '#fff',
        backgroundColor: '#FFFF',
    },
    textTouchLabel: {
        fontWeight: '600',
        fontSize: 10.5,
        // lineHeight: 15,
        textTransform: 'uppercase',
        color: '#91929e',
    },
    textTouchProperty: {
        fontWeight: 'bold',
        fontSize: 17,
        lineHeight: 20,
        color: '#24253D',
    },

    viewFilterOption: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        backgroundColor: '#ffff'
    },
    touchFilterOption: {
        paddingVertical: 11,
    },
    viewSubTouchOption: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    viewListResult: {
        marginTop: 5,
        flex: 1
    },

});