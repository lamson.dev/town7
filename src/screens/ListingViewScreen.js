import React, { Component } from "react";
import { View, StyleSheet, FlatList, Text } from "react-native";
import { Navigation } from "react-native-navigation";

import TextInputComp from "../components/TextInputComp";
import RoomService from "../services/room.service";
import DetailRoomScreen from "./DetailRoomScreen";
import { mStyles } from "../styles/styles";
import CardHostList from "../components/CardHostList";
import LoaderModal from "../components/LoaderModal";

export default class ListingViewScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            typeView: props.typeView,
            hostCityId: props.cityId,
            hostResultData: [],
            loading: false
        };
    }

    gotoRoomDetail = (roomId) => {
        Navigation.registerComponent('DetailRoomScreen', () => DetailRoomScreen)
        Navigation.push(this.props.componentId, {
            component: {
                name: 'DetailRoomScreen',
                passProps: {
                    roomId: roomId
                },
                options: {
                    bottomTabs: {
                        visible: false
                    },
                    topBar: {
                        backButton: {
                            color: '#ffff',
                        },
                        drawBehind: true,
                        background: {
                            color: 'transparent'
                        },
                        noBorder: true,
                        elevation: 0,
                    }
                }
            }
        });
    }

    renderCardResult = (host) => {
        return (
            <FlatList
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={host}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={
                    ({ item }) =>
                        <CardHostList
                            onPress={() => this.gotoRoomDetail(item.id)}
                            hostName={item.name}
                            imgUri={"https://ezcloud.vn/wp-content/uploads/2019/03/kinh-doanh-homestay-kiem-tien-trieu.jpg"}
                            price={item.standardPriceMondayToThursday}
                            starVoting={item.stars ? Math.round(item.stars * 10) / 10 : 0}
                            address={item.address}
                            bedroomCount={item.bedroomCount}
                            totalReview={item.totalReview} />
                }
            />
        )
    }

    async componentDidMount() {
        if (this.state.typeView == 'all') {
            this.setState({ loading: true });
            await RoomService.getAll().then(response => {
                console.log(response.data);
                this.setState({
                    loading: false,
                    hostResultData: response.data.data
                });
            }).catch(error => {
                this.setState({ loading: false });
                alert('Error: ' + error);
            })
            this.setState({ loading: false });
        } else {
            console.log('id city: ', this.state.hostCityId);
            this.setState({ loading: true });
            await RoomService.getByCity(this.state.hostCityId).then(response => {
                console.log(response.data);
                this.setState({
                    loading: false,
                    hostResultData: response.data.data.data
                });
            }).catch(error => {
                this.setState({ loading: false });
                alert('Error: ' + error);
            })
            this.setState({ loading: false });

        }
    }

    render() {
        return (
            <View style={[mStyles.bkgColorMain, styles.viewMain]}>
                <LoaderModal
                    loading={this.state.loading} />

                {/* List view of result */}
                <View style={styles.viewListResult}>
                    {/* card view */}
                    {this.state.hostResultData ? this.renderCardResult(this.state.hostResultData) : <Text>not found any result</Text>}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewMain: {
        flex: 1,
    },
    viewTop: {
        width: "100%",
        marginBottom: 3.5,
        backgroundColor: '#24c3f0',
        paddingTop: 30
    },

    viewCheckInOutRoom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 2.5,
        marginHorizontal: 20
    },
    touchProperty: {
        marginBottom: 5,
        paddingLeft: 9,
        width: 112,
        paddingVertical: 5,
        justifyContent: 'center',
        borderWidth: 0.7,
        borderRadius: 8,
        borderColor: '#fff',
        backgroundColor: '#FFFF',
    },
    textTouchLabel: {
        fontWeight: '600',
        fontSize: 10.5,
        // lineHeight: 15,
        textTransform: 'uppercase',
        color: '#91929e',
    },
    textTouchProperty: {
        fontWeight: 'bold',
        fontSize: 17,
        lineHeight: 20,
        color: '#24253D',
    },

    viewFilterOption: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
        backgroundColor: '#ffff'
    },
    touchFilterOption: {
        paddingVertical: 11,
    },
    viewSubTouchOption: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },

    viewListResult: {
        marginTop: 5,
        flex: 1
    },

});