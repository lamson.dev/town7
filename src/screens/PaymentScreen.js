import React, { Component } from "react";
import {
    Alert,
    ScrollView,
    Platform, StyleSheet, Text, View, TouchableOpacity, TextInput, DeviceEventEmitter,
    SafeAreaView, Image, NativeModules, NativeEventEmitter, ActivityIndicator,
    Linking,
    Button
} from "react-native";
import { Navigation } from "react-native-navigation";
import PaymentService from "../services/payment.service";
import LoaderModal from "../components/LoaderModal";

import RNMomosdk from 'react-native-momosdk';
const RNMomosdkModule = NativeModules.RNMomosdk;
const EventEmitter = new NativeEventEmitter(RNMomosdkModule);

import ButtonMain from "../components/ButtonMain";

const merchantname = "Town 7";
const merchantcode = "MOMOGYJC20200615";
const merchantNameLabel = "Town 7";
const billdescription = "Thanh toán đặt phòng";
const amount = 50000;
const enviroment = "0"; //"0": SANBOX , "1": PRODUCTION

export default class PaymentScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bookingData: props.bookingData,
            textAmount: this.formatNumberToMoney(amount, null, ""),
            amount: amount,
            description: "",
            processing: false,
            loading: false
        }
    }

    formatNumberToMoney(number, defaultNum, predicate) {
        predicate = !predicate ? "" : "" + predicate;
        if (number == 0 || number == '' || number == null || number == 'undefined' ||
            isNaN(number) === true ||
            number == '0' || number == '00' || number == '000')
            return "0" + predicate;

        var array = [];
        var result = '';
        var count = 0;

        if (!number) {
            return defaultNum ? defaultNum : "" + predicate
        }

        let flag1 = false;
        if (number < 0) {
            number = -number;
            flag1 = true;
        }

        var numberString = number.toString();
        if (numberString.length < 3) {
            return numberString + predicate;
        }

        for (let i = numberString.length - 1; i >= 0; i--) {
            count += 1;
            if (numberString[i] == "." || numberString[i] == ",") {
                array.push(',');
                count = 0;
            } else {
                array.push(numberString[i]);
            }
            if (count == 3 && i >= 1) {
                array.push('.');
                count = 0;
            }
        }

        for (let i = array.length - 1; i >= 0; i--) {
            result += array[i];
        }

        if (flag1)
            result = "-" + result;

        return result + predicate;
    }


    async componentDidMount() {
        // Listen for native events
        let me = this;
        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenReceived', (response) => {
            console.log("<MoMoPay>Listen.Event::" + JSON.stringify(response));
            try {
                if (response && response.status == 0) {
                    let fromapp = response.fromapp; //ALWAYS:: fromapp==momotransfer
                    me.setState({ description: JSON.stringify(response), processing: false });
                    let momoToken = response.data;
                    let phonenumber = response.phonenumber;
                    let message = response.message;
                    let orderId = response.refOrderId; //your orderId
                    let requestId = response.refRequestId; //your requestId
                    //continue to submit momoToken,phonenumber to server
                } else {
                    console.log('response momo fail: ', response);
                    me.setState({ description: "message: Get token fail", processing: false });
                }
            } catch (ex) { }

        });

        //OPTIONAL
        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenState', (response) => {
            console.log("<MoMoPay>Listen.RequestTokenState:: " + response.status);
            // status = 1: Parameters valid & ready to open MoMo app.
            // status = 2: canOpenURL failed for URL MoMo app 
            // status = 3: Parameters invalid
        })
    }

    confirmPayment = async () => {
        if (!this.state.processing) {
            let jsonData = {};
            jsonData.enviroment = "0"; //"0": SANBOX , "1": PRODUCTION
            jsonData.action = "gettoken";
            jsonData.isDev = true; //SANBOX only , remove this key on PRODUCTION 
            jsonData.merchantname = merchantname;
            jsonData.merchantcode = merchantcode;
            jsonData.merchantnamelabel = merchantNameLabel;
            jsonData.description = billdescription;
            jsonData.amount = this.state.bookingData.totalPrice;
            jsonData.orderId = this.state.bookingData.bookingCode;
            // jsonData.requestId = "1234";
            // jsonData.orderLabel = "Thanh toan dich vu Town 7";
            // jsonData.appScheme = "momocgv20170101";// iOS App Only , get from Info.plist > key URL types > URL Schemes. Check Readme
            console.log("data_request_payment " + JSON.stringify(jsonData));
            if (Platform.OS === 'android') {
                this.setState({ processing: true });
                let dataPayment = await RNMomosdk.requestPayment(jsonData);
                this.momoHandleResponse(dataPayment);
            } else {
                RNMomosdk.requestPayment(JSON.stringify(jsonData));
            }
            this.setState({ description: "", processing: false });
        }
        else {
            this.setState({ description: ".....", processing: false });
        }
    }

    async momoHandleResponse(response) {
        console.log('response momo: ', response);
        try {
            if (response && response.status == 0) {
                console.log('Transaction successful')
                let fromapp = response.fromapp; //ALWAYS:: fromapp==momotransfer
                this.setState({ processing: false });
                let momoToken = response.data;
                let phonenumber = response.phonenumber;
                let message = response.message;
                //continue to submit momoToken,phonenumber to server

                this.setState({ loading: true });
                await PaymentService.momoPayment({
                    "amount": response.amount,
                    "data": response.data,
                    "message": response.message,
                    "bookingCode": response.orderId,
                    "phoneNumber": response.phonenumber,
                    "description": response.description,
                    "status": response.status
                }).then(() => {
                    this.setState({ loading: false });
                    this.showSuccessfulPaymentrModal();
                }).catch(error => {
                    this.setState({ loading: false });
                    console.log('api error: ', error);
                    alert("Error: " + error);
                })
                this.setState({ loading: false });

            } else {
                console.log('Transaction Failed')
                this.setState({ description: "message: Transaction Failed", processing: false });
            }
        } catch (ex) {
            alert("Error: can not payment your booking")

        }
    }

    showSuccessfulPaymentrModal = () => {
        Navigation.showModal({
            stack: {
                children: [
                    {
                        component: {
                            name: "SuccessfulPaymentScreen"
                        }
                    }
                ]
            }
        })
        Navigation.pop(this.props.componentId);
    }

    render() {
        return (

            <View style={styles.container}>
                <LoaderModal
                    loading={this.state.loading} />
                <ScrollView>
                    {/* header payment */}
                    <View style={styles.viewHeadPayment}>
                        <Text>Summary</Text>

                        <View style={styles.viewFeeTaxSub}>
                            <Text style={{ color: 'grey' }}>
                                Room price
                            </Text>
                            <Text style={{ color: 'grey' }}>
                                {this.state.bookingData.pricePerNight} đ
                            </Text>
                        </View>

                        <View style={styles.viewFeeTaxSub}>
                            <Text style={{ color: 'grey' }}>
                                Room x 4 night
                            </Text>
                            <Text style={{ color: 'grey' }}>
                                {(this.state.bookingData.nights * this.state.bookingData.pricePerNight)} đ
                            </Text>
                        </View>

                        <View style={styles.viewFeeTaxSub}>
                            <Text style={{ color: 'grey' }}>
                                Cleaning fee
                            </Text>
                            <Text style={{ color: 'grey' }}>
                                {this.state.bookingData.cleanCosts} đ
                            </Text>
                        </View>

                        <View style={styles.viewFeeTaxSub}>
                            <Text style={{ color: 'grey' }}>
                                Service fee
                                </Text>
                            <Text style={{ color: 'grey' }}>
                                {this.state.bookingData.serviceCharge} đ
                            </Text>
                        </View>

                        <View style={styles.viewTotalFee}>
                            <Text style={styles.textTotalFee}>
                                Total:
                                </Text>
                            <Text style={[styles.textTotalFee, { fontWeight: 'bold', color: 'red' }]}>
                                {this.state.bookingData.totalPrice} đ
                            </Text>
                        </View>

                    </View>

                    {/* body paypment */}
                    <View style={styles.viewBodyPayment}>

                        <Text style={styles.textPaymentMethod}>
                            Payment method
                            </Text>

                        {/* Touch payment methods */}
                        <View style={styles.viewButtonPaymenMethod}>

                            <TouchableOpacity style={[styles.touchPaymentMethod, { backgroundColor: '#24c3f0' }]}>
                                <View style={styles.viewTouchPayment}>
                                    <Image style={styles.imgTouchPayment} source={require('../assets/images/icons/logo-momo-ic.png')} />
                                    <Text style={[styles.textTouchPayment, { color: '#fff' }]}>Momo E-wallet</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={[styles.touchPaymentMethod, { backgroundColor: '#fff' }]}>
                                <View style={styles.viewTouchPayment}>
                                    <Image style={styles.imgTouchPayment} source={require('../assets/images/icons/credit-ic.png')} />
                                    <Text style={[styles.textTouchPayment]}>Credit</Text>
                                </View>
                            </TouchableOpacity>
                        </View>

                        {/* Detail Payment Method */}
                        <View style={styles.viewDetailPaymentMethod}>

                            <View style={styles.viewField}>
                                <Text style={styles.textLabelField}>Booking Code</Text>
                                <Text style={styles.textField}>{this.state.bookingData.bookingCode}</Text>
                            </View>
                            <View style={styles.viewField}>
                                <Text style={styles.textLabelField}>Merchant Name</Text>
                                <Text style={styles.textField}>{merchantname}</Text>
                            </View>
                            <View style={styles.viewField}>
                                <Text style={styles.textLabelField}>Description</Text>
                                <Text style={styles.textField}>{billdescription}</Text>
                            </View>

                        </View>

                    </View>


                </ScrollView>
                <View style={styles.viewSignInBtn}>

                    <TouchableOpacity disabled={this.state.processing} onPress={this.confirmPayment} style={styles.touchSignIn} >
                        {
                            this.state.processing ?
                                <Text style={styles.textGrey}>Waiting verification from MoMo App</Text>
                                :
                                <Text style={styles.text}>COMPLETE PAYMENT</Text>
                        }
                    </TouchableOpacity>
                    {this.state.processing ?
                        <ActivityIndicator size="small" color="#000" />
                        : null
                    }
                    {
                        this.state.description != "" ?
                            <Text style={[styles.text, { color: 'red' }]}>{this.state.description}</Text>
                            : null
                    }
                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
    },
    container: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    viewHeadPayment: {
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 2,
    },
    viewFeeTaxSub: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // backgroundColor: 'blue',
        marginVertical: 5,
        paddingHorizontal: 20,
    },
    viewTotalFee: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5
    },
    textTotalFee: {
        fontSize: 18.5,
        fontWeight: '500'
    },
    viewButtonBook: {
        paddingBottom: 7,
        marginHorizontal: 19,
    },
    textInput: {
        fontSize: 16,
        marginHorizontal: 15,
        marginTop: 5,
        height: 40,
        paddingBottom: 2,
        borderBottomColor: '#dadada',
        borderBottomWidth: 1,
    },
    formInput: {
        backgroundColor: '#FFF',
        borderBottomColor: '#dadada',
        borderTopColor: '#dadada',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        paddingBottom: 5,
    },
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,
        backgroundColor: '#b0006d',
        borderRadius: 4,
        marginHorizontal: 40,
        marginVertical: 10
    },
    text: {
        color: "#FFFFFF",
        fontSize: 18,
        textAlign: 'center',
        paddingHorizontal: 10
    },
    textGrey: {
        color: "grey",
        fontSize: 18,
        textAlign: 'center',
    },
    viewSignInBtn: {
        paddingBottom: 10,
    },
    touchSignIn: {
        height: 56,
        backgroundColor: '#24c3f0',
        justifyContent: 'center',
        borderRadius: 26.5,

        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 0.5 * 5
        },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * 5,
        elevation: 5,
    },
    textSignInTouch: {
        color: '#FFFFFF',
        fontSize: 21.2,
        fontWeight: "normal",
        fontStyle: 'normal',
        textAlign: 'center'
    },

    viewBodyPayment: {
        paddingHorizontal: 20,
        paddingTop: 10
    },
    textPaymentMethod: {
        fontSize: 17.5,
        fontWeight: 'bold'
    },
    viewButtonPaymenMethod: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 10,
    },
    touchPaymentMethod: {
        width: 150,
        height: 50,
        borderWidth: 1,
        borderColor: '#24c3f0',
        borderRadius: 5,

        shadowColor: "black",
        shadowOffset: {
            width: 0,
            height: 0.5 * 5
        },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * 5,
        elevation: 2.5,
    },
    viewTouchPayment: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    imgTouchPayment: {
        width: 30,
        height: 30
    },
    textTouchPayment: {
        fontWeight: '100',
    },
    viewDetailPaymentMethod: {
        backgroundColor: '#fff',
        borderRadius: 5,
        paddingVertical: 5,
        paddingHorizontal: 8.5,
        shadowOffset: {
            width: 0,
            height: 0.5 * 5
        },
        elevation: 5,

    },
    viewField: {
        borderBottomWidth: 0.5,
        borderColor: 'grey',
        paddingBottom: 10
    },
    textLabelField: {
        fontSize: 14,
        color: 'grey'
    },
    textField: {
        fontSize: 17.5,
        fontWeight: "700"
    }
});