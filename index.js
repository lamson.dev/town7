/**
 * @format
 */
import { Navigation } from "react-native-navigation"
import messaging from '@react-native-firebase/messaging';
// import {AppRegistry} from 'react-native';
import App from './App';
// import App from './src/screens/SuccessfulRegisterScreen';
// import {name as appName} from './app.json';
import { registerScreen } from "./src/navigator/registers";

// AppRegistry.registerComponent(appName, () => App);
Navigation.registerComponent(`indexScr`, () => App);

registerScreen();

messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
})

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            component: {
                name: "indexScr"
            }
        }
    });
});